# ------------------------------------------------------------------------------

# The name of the library.
THIS     := inferno

# The name of the library, capitalized.
MODULE   := Inferno

# The version number is automatically set to the current date,
# unless DATE is defined on the command line.
DATE     := $(shell /bin/date +%Y%m%d)

# The repository URL (https).
REPO     := https://gitlab.inria.fr/fpottier/$(THIS)

# The archive URL (https).
ARCHIVE  := $(REPO)/-/archive/$(DATE)/archive.tar.gz

# ------------------------------------------------------------------------------

.PHONY: all
all:
	@ dune build @all

# [make test] currently requires about 15 seconds.

.PHONY: test
test:
	@ dune runtest --force && \
	  cat _build/default/client/test/_build/_tests/random_suite/*.output | grep well-typed

# [make quick] is a subset of [make test] and requires about 1 second.

.PHONY: quick
quick: all
	@ INFERNO_SLOW_TESTS=0 dune runtest --force

# [make bench] is a performance micro-benchmark; not part of [make test].

.PHONY: bench
bench:
	@ dune build client/test/BenchMLRandom.exe
	@ _build/default/client/test/BenchMLRandom.exe

.PHONY: install
install: all
	@ dune install -p $(THIS)

.PHONY: clean
clean:
	@ rm -f *~ src/*~
	@ dune clean

.PHONY: uninstall
uninstall:
	@ dune build @install
	@ dune uninstall -p $(THIS)

.PHONY: show
show: install
	@ echo "#require \"$(THIS)\";;\n#show $(MODULE);;" | ocaml

.PHONY: pin
pin:
	@ opam pin add $(THIS) .

.PHONY: unpin
unpin:
	@ opam pin remove $(THIS)

# ------------------------------------------------------------------------------

# [make versions] compiles Inferno under many versions of OCaml, whose
# list is specified below.

# This requires appropriate opam switches to exist. A missing switch
# can be created like this:
#   opam switch create 4.03.0

VERSIONS := \
  4.08.1 \
  4.09.1 \
  4.09.0+bytecode-only \
  4.10.0 \
  4.11.1 \
  4.12.0 \
  4.13.1 \
  4.14.0 \

.PHONY: versions
versions:
	@(echo "(lang dune 2.0)" && \
	  for v in $(VERSIONS) ; do \
	    echo "(context (opam (switch $$v)))" ; \
	  done) > dune-workspace.versions
	@ dune build --workspace dune-workspace.versions @all

.PHONY: handiwork
handiwork:
	@ current=`opam switch show` ; \
	  for v in $(VERSIONS) ; do \
	    opam switch $$v && \
	    eval $$(opam env) && \
	    opam install --yes qcheck-alcotest ; \
	  done ; \
	  opam switch $$current

# ------------------------------------------------------------------------------

# [make doc] compiles the documentation using odoc.
# [make view] opens the documentation in a browser.
# [make export] publishes the documentation on François' Web page.

DOCDIR = _build/default/_doc/_html
DOC    = $(DOCDIR)/inferno/index.html

.PHONY: doc
doc:
	@ rm -rf _build/default/_doc
	@ dune clean
	@ dune build @doc 2>&1 | ./doc.sh
	@ echo "You can view the documentation by typing 'make view'".

.PHONY: view
view: doc
	@ echo Attempting to open $(DOC)...
	@ if command -v firefox > /dev/null ; then \
	  firefox $(DOC) ; \
	else \
	  open -a /Applications/Firefox.app/ $(DOC) ; \
	fi

.PHONY: export
export: doc
	ssh yquem.inria.fr rm -rf public_html/$(THIS)/doc
	scp -r $(DOCDIR) yquem.inria.fr:public_html/$(THIS)/doc

# ------------------------------------------------------------------------------

# [make headache] updates the header in every source file.

HEADACHE := headache
LIBHEAD  := $(shell pwd)/headers/library-header
FIND     := $(shell if command -v gfind >/dev/null ; then echo gfind ; else echo find ; fi)

.PHONY: headache
headache:
	@ $(FIND) src -regex ".*\.ml\(i\|y\|l\)?" \
	    -exec $(HEADACHE) -h $(LIBHEAD) "{}" ";"

# ------------------------------------------------------------------------------

# [make release] tags a new release and uploads the new tag on gitlab.

.PHONY: release
release:
# Make sure the current version can be compiled and installed.
	@ make uninstall
	@ make clean
	@ make install
# Check the current package description.
	@ opam lint
# Check if everything has been committed.
	@ if [ -n "$$(git status --porcelain)" ] ; then \
	    echo "Error: there remain uncommitted changes." ; \
	    git status ; \
	    exit 1 ; \
	  else \
	    echo "Now making a release..." ; \
	  fi
# Create a git tag.
	@ git tag -a $(DATE) -m "Release $(DATE)."
# Upload. (This automatically makes a .tar.gz archive available on gitlab.)
	@ git push
	@ git push --tags
# Done.
	@ echo "Done."
	@ echo "If happy, please type:"
	@ echo "  \"make publish\"   to publish a new opam package"
	@ echo "  \"make export\"    to upload the documentation to yquem.inria.fr"

.PHONY: publish
publish:
# Publish an opam description.
	@ opam publish -v $(DATE) $(THIS) $(ARCHIVE) .
