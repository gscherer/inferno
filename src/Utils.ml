(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

(* [r++]. *)

let postincrement r =
  let v = !r in
  r := v + 1;
  v

(* Generating fresh integer identifiers. *)

let gensym () =
  let r = ref 0 in
  fun () ->
    postincrement r
