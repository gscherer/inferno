{0 Inferno}

Inferno is an OCaml library for {b constraint-based type inference
and elaboration}. Its constraint solver performs {b first-order
unification} and handles {b Hindley-Milner polymorphism}. The
constraints carry {b semantic actions} that facilitate elaboration,
which is the process of constructing an explicitly-typed term.

{1 Public API}

The constraint solver API
provided by the functor {!Inferno.Solver.Make}
is public.

{1 Private API}

The documentation of the following modules is made available as an aid
to understanding the code. However, these modules should {i not}
directly be used outside Inferno; their API is subject to change.

- term {{!Inferno.Structure}structure} that the unifier can exploit;
- the {{!Inferno.Unifier.Make}unifier};
- the {{!Inferno.OccursCheck.Make}occurs check};
- the {{!Inferno.Decoder.Make}type decoders};
- the {{!Inferno.Generalization.Make}generalization} machinery.

{1 History and Acknowledgements}

Inferno has been written by
{{:http://cambium.inria.fr/~fpottier/}François Pottier} and is
described in the paper
{{:http://cambium.inria.fr/~fpottier/publis/fpottier-elaboration.pdf}Hindley-Milner
elaboration in applicative style} (ICFP 2014).

Since 2020, Inferno has benefited from many contributions by Olivier
Martinot and {{:http://gallium.inria.fr/~scherer/}Gabriel Scherer}.
See in particular
{{:https://tel.archives-ouvertes.fr/X-DEP-INFO/hal-03145040v1}Quantified
Applicatives: API design for type-inference constraints} (ML Family
Workshop 2020).
