(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

open Signatures

(**This functor transforms a type ['a structure], equipped with [map],
   [iter] and [conjunction] operations, into the type ['a structure option],
   equipped with the same operations. The type ['a structure option] is
   typically useful when implementing some form of unification of terms:
   indeed, the optional structure [None] indicates the absence of a
   constraint, while the optional structure [Some term] indicates the
   presence of an equality constraint. *)
module Option (S : sig (** @inline *) include GSTRUCTURE end) : sig

  (** @inline *)
  include GSTRUCTURE_OPT with module S = S

end
