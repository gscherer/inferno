(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

open Signatures

module Make (S : GSTRUCTURE_OPT) = struct

(* -------------------------------------------------------------------------- *)

(* With every equivalence class (of unification variables), we associate a
   unique identifier. These identifiers allow us to use hash tables whose
   keys are equivalence classes. Of course, these tables must be used only
   while the unifier is inactive. *)

let fresh_id : unit -> id =
  Utils.gensym()

(* -------------------------------------------------------------------------- *)

(* With every equivalence class, we associate a structure. A leaf structure
   means that no constraint bears on this equivalence class; it really
   represents "a variable". A nonleaf structure means that an equality
   constraint bears on this equivalence class. *)

(* -------------------------------------------------------------------------- *)

(* With every equivalence class, we associate a integer rank. We also maintain
   a global "current rank", stored in the field [state.young]. *)

(* Ranks can be thought of as de Bruijn levels. Indeed, whenever the left-hand
   side of a [CLet] constraint is entered, the current rank is incremented by
   one. Thus, the rank of a variable indicates where (that is, at which [CLet]
   construct) this variable is bound. *)

(* [base_rank] is the smallest rank. We set [base_rank = 0], so every rank is
   nonnegative. We take advantage of this property and use ranks as indices
   into the infinite array [state.pool]. *)

type rank =
  int

let base_rank =
  0


(* -------------------------------------------------------------------------- *)

(* We want to be able to mark an equivalence class as either "active" or
   "generic". The unifier works with active variables only; it never sees a
   generic variable. Generic variables are used as part of the representation
   of schemes: when we create a scheme, we mark the variables that can be
   generalized as generic; when we instantiate a scheme, we replace every
   generic variable with a fresh active variables. *)

(* The generic variables always form a prefix of the structure: the children
   of a generic variable can be generic or active; the children of an
   active variable must be active. *)

(* An active variable is intended to become generic some day. A generic
   variable never becomes active again. *)

(* An active variable is either flexible or generic. *)

(* A rigid variable has a fixed rank -- lowering its rank is an error --
   and a fixed leaf structure -- unifying it with a non-leaf structure
   is an error. *)

type status =
  | Rigid
  | Flexible
  | Generic

(* -------------------------------------------------------------------------- *)

(* We let each equivalence class carry a mutable integer mark. This [mark]
   field is transient: it does not carry long-lived information. It is used
   locally, in various places, to store short-lived information. In several
   places, it allows us to save the need for a hash table. *)

(* As long as a class is active, the content of the [mark] field must be a
   value that has been generated in the past by [fresh_mark()]. This ensures
   that we can generate a new mark using [fresh_mark()] and that this new mark
   is distinct from the value of every [mark] field. This is exploited in the
   functions [update_ranks] and [schemify]. *)

(* Once a class becomes generic, we switch to a different use of the [mark]
   field. This field is allowed to contain arbitrary integer values. It is
   used by [instantiate] to (temporarily) store an index into an array. *)

(* This convention relies on the fact that a generic variable never becomes
   active again. *)

type mark =
  int

let fresh_mark : unit -> mark =
  Utils.gensym()

(* -------------------------------------------------------------------------- *)

(* The type ['a data] groups all of the information that we associate with an
   equivalence class. *)

(* The type parameter ['a] is the type of the children. *)

(* We maintain the property that each equivalence class uniquely owns the
   [data] record that is associated with it. This allows us to make this
   record mutable and to update this record in place. However, in-place
   updates are permitted *only while the unifier is inactive*. While the
   unifier is running, one must not perform any side effects, because the
   unifier restores its initial state when a unification error occurs. *)

type 'a data = {
  id: id;
  mutable structure: 'a S.structure;
  mutable rank: rank;
  mutable status: status;
  mutable mark: mark;
}

(* A printer (for debugging purposes). *)

let pprint elem data =
  let open PPrint in
  utf8format "@%d[%d]" data.id data.rank ^^
  S.pprint elem data.structure
    (* [status] and [mark] are currently not printed. *)

(* This exception is raised when a rigid variable escapes its scope, that is,
   when it is unified with (or becomes a child of) a variable of lesser rank.
   At present, this exception does not carry any data; we do not yet know how
   to produce a good error message in this situation. *)

exception VariableScopeEscape

(* If [data.status] is flexible, then [adjust_rank data k] is equivalent
   to [data.rank <- min k data.rank]. If [data.status] is rigid, then it
   causes an error. Indeed, the rank of a rigid variable is fixed; it can
   never decrease. *)

let adjust_rank (data : 'a data) (k : rank) =
  assert (data.status <> Generic);
  if k < data.rank then
    if data.status = Flexible then
      data.rank <- k
    else
      raise VariableScopeEscape

(* The module [Data] is meant to be an argument for [Unifier.Make]. *)

module Data = struct

  type 'a structure =
    'a data

  let pprint = pprint

  let id data =
    data.id

  let dummy : mark =
    fresh_mark()

  let make structure rank status =
    let id = fresh_id() in
    let mark = dummy in
    { id; structure; rank; status; mark }

  let map f data =
    { data with structure = S.map f data.structure }

  let iter f data =
    S.iter f data.structure

  let fold f data accu =
    S.fold f data.structure accu

  exception InconsistentConjunction =
    S.InconsistentConjunction

  (* [conjunction] is invoked by the unifier when two equivalence classes are
     unified. It is in charge of computing the data associated with the new
     class. *)

  (* As explained above, *no imperative updates* must be performed inside this
     function, because it is invoked while the unifier is running. Thus, we
     must allocate a new [data] record, and must not update [data1] or
     [data2]. *)

  (* We may choose the identifier of the new class however we wish, provided
     we are able to guarantee that it is unique. Thus, we can either re-use
     one of the existing identifiers [data1.id] and [data2.id], or generate a
     fresh identifier. Here, we keep an arbitrary identifier. *)

  let conjunction equate data1 data2 =
    let id = data1.id in
    (* The new structural constraint is the logical conjunction of the two
       structural constraints. *)
    let structure = S.conjunction equate data1.structure data2.structure in
    (* The new rank is the minimum of the two ranks. *)
    let rank = min data1.rank data2.rank in
    (* If either variable is rigid, the conjunction is also rigid. However,
       unification in the presence of rigid variables is subject to certain
       restrictions. *)
    let status : status =
      match data1.status, data1.rank, data2.status, data2.rank with
      | Generic, _, _, _
      | _, _, Generic, _ ->
          assert false
      | Flexible, _, Flexible, _ ->
          Flexible
      | Rigid, _, Rigid, _ ->
          (* One cannot unify two rigid variables. *)
          raise InconsistentConjunction
      | Rigid, rigid_rank, Flexible, _
      | Flexible, _, Rigid, rigid_rank ->
          (* One cannot lower the rank of a rigid variable. *)
          if rank < rigid_rank then raise VariableScopeEscape;
          (* One cannot assign some structure to a rigid variable. *)
          if Option.is_some structure then raise InconsistentConjunction;
          Rigid
    in
    (* There is no need to preserve marks during unification. *)
    let mark = dummy in
    { id; structure; rank; status; mark }

  let is_leaf data =
    Option.is_none data.structure

  let project data =
    data.structure

end

(* -------------------------------------------------------------------------- *)

(* Set up the unifier and tell it to associate the above data with every
   equivalence class of variables. *)

module U =
  Unifier.Make(Data)

type variable =
  U.variable

let get : variable -> variable data =
  U.get

(* Set up an occurs check. *)

module OC =
  OccursCheck.Make(Data)(U)

exception Cycle =
  OC.Cycle

(* -------------------------------------------------------------------------- *)

(* The rank of a variable is set to the current rank when the variable is
   created. During the lifetime of a variable, its rank can only decrease.
   Decreasing a variable's rank amounts to hoisting out the existential
   quantifier that binds this variable. *)

(* Ranks are updated in a lazy manner. During unification, when two
   equivalence classes are unified, the rank of the new class is set to the
   minimum of the ranks of the two variables. (This operation is performed for
   us by the unifier.) Two other rank maintenance operations are performed
   here, namely downward propagation and upward propagation. Downward
   propagation updates a child's rank, based on its father's rank; there is no
   need for a child's rank to exceed its father's rank. (Indeed, the father
   determines every child.) Upward propagation updates a father's rank, based
   the ranks of all of its children: there is no need for a father's rank to
   exceed the maximum of its children's ranks. (Indeed, the children together
   determine the father.) These operations are performed at generalization
   time because it would be costly (and it is unnecessary) to perform them
   during unification. *)

(* The [rank] field maps every variable to the [CLet] construct where it is
   bound. Conversely, we maintain, for every active [CLet] construct, a list
   of all variables that are bound there. This takes the form of an infinite
   array, stored in the field [state.pool]. For every rank comprised between
   [base_rank] and [state.young], both included, this array stores a list of
   the variables that are bound there. This array is updated in a lazy manner,
   at generalization time. Because the unifier updates the ranks, but does not
   know about this array, the property that holds in general is: if a variable
   [v] has rank [i], then it appears in pool number [j], where [i <= j] holds.
   Immediately after generalization has been performed, the array has been
   updated, so [i = j] holds. *)

type state = {

  (* An infinite array of pools (lists of variables), indexed by ranks. *)
  pool: variable list InfiniteArray.t;

  (* The current rank. *)
  mutable young: int;

}

(* -------------------------------------------------------------------------- *)

(* The initial state. *)

(* There is no strong reason to explicitly carry the [state] record around,
   so we allocate it at the top level. *)

(* The pool array, which is conceptually infinite, is initially populated with
   an empty pool at every rank. The initial value of [state.young] is chosen
   so that the first rank that is actually exploited is [base_rank]. *)

let state =
  let pool = InfiniteArray.make 8 []
  and young = base_rank - 1 in
  { pool; young }

(* [register v] assumes that the rank of [v] is [r] and that it is a valid
   rank. It registers [v] by inserting it into the appropriate pool. *)

let register v r =
  let pool = state.pool in
  InfiniteArray.set pool r (v :: InfiniteArray.get pool r)

(* When a fresh unification variable is created, it receives the current rank,
   and it is immediately registered at this rank. *)

let flexible structure =
  let r = state.young in
  let status = Flexible in
  let v = U.fresh (Data.make structure r status) in
  register v r;
  v

let rigid () =
  let r = state.young in
  let status = Rigid in
  let v = U.fresh (Data.make None r status) in
  register v r;
  v

(* -------------------------------------------------------------------------- *)

(* [enter] increments the current rank, in preparation for a matching call to
   [exit], where generalization is performed. *)

let enter () =
  (* Increment the current rank. *)
  state.young <- state.young + 1;
  (* The pool associated with the updated current rank must be empty. *)
  assert (InfiniteArray.get state.pool state.young = []);

(* The sibling function, [exit], is much more complex. *)

(* -------------------------------------------------------------------------- *)

(* The following functions are auxiliary functions that take part in the
   definition of [exit]. They perform the generalization work over the young
   variables. In short, this work consists in examining the young variables
   and adjusting their rank. The variables whose rank becomes lower than
   [state.young] are not generalized; they are re-registered in lower pools.
   The variables whose rank remains equal to [state.young] can be generalized;
   they are marked generic. *)

(* A record of type [generation] holds (several representations of) the set of
   all young variables. A variable is "young" if it is currently registered in
   the youngest pool. This implies that its rank is at most [state.young]. It
   can have a strictly lower rank. *)

(* The use of a [generation] record is not strictly necessary, but allows us
   to break up the following code in manageable chunks. *)

type generation = {

  (* A list of all young variables. This list can contain several members
     of a single equivalence class. *)
  inhabitants: variable list;

  (* An array of all young variables, indexed by rank. This array of lists
     contains the same elements as the list [inhabitants]. *)
  by_rank: variable list array;

  (* A function that determines whether a variable is young, up to
     equivalence. That is, [is_young v] returns [true] if [v] is in
     the same equivalence class as some young variable [v']. *)
  is_young: variable -> bool;

}

(* -------------------------------------------------------------------------- *)

(* [discover_young_generation] is invoked when we are about to exit the
   left-hand side of a [CLet] construct. It creates a [generation] record
   that describes the young generation. *)

let discover_young_generation () =

  (* The most recent pool holds a list of all variables in the young
     generation. *)
  let inhabitants = InfiniteArray.get state.pool state.young in

  (* Allocate a table that lets us mark the equivalence classes that have a
     young inhabitant. (We cannot use the mutable field [mark], because
     [update_ranks] simultaneously uses this field and uses [is_young].
     We could use another mutable field, but we choose not to.) *)
  let guess = (List.length inhabitants)/4 in
  let table = Hashtbl.create guess in

  (* Allocate an array that lets us sort the young variables by rank. *)
  let by_rank = Array.make (state.young + 1) [] in

  (* Populate [table] and [by_rank]. *)
  inhabitants |> List.iter (fun v ->
    (* We do NOT test whether [table] already holds a variable that lies
       in the same class as [v]. Thus, we do not eliminate duplicates;
       we want [by_rank] to contain the same elements as [inhabitants].
       We use [replace], as opposed to [add], because there is no point
       in storing repeated entries in [table]. *)
    let data = get v in
    Hashtbl.replace table data.id ();
    let r = data.rank in
    assert (data.status <> Generic);
    assert (base_rank <= r && r <= state.young);
    by_rank.(r) <- v :: by_rank.(r)
  );

  (* Define an accessor for [table]. *)
  let is_young v = Hashtbl.mem table (get v).id in

  (* Done. *)
  { inhabitants; by_rank; is_young }

(* -------------------------------------------------------------------------- *)

(* [update_ranks] updates the rank of every variable in the young generation.

   Downward propagation and upward propagation, which have been described
   earlier, are performed. A single depth-first traversal of the young
   generation achieves both. Roughly speaking, downward propagation is
   achieved on the way down, while upward propagation is achieved on the way
   up. (In reality, all rank updates takes place during the upward phase.)

   Downward propagation is necessary. It ensures that a variable whose rank is
   less than [state.young] cannot have children whose rank is [state.young].
   When generalization takes place, a variable whose rank is [state.young] is
   turned into a generic variable. Thus, after generalization, a variable
   whose rank is less than [state.young] cannot have generic children. This is
   an invariant that we must preserve.

   Upward propagation is not necessary: it is an optional optimization.
   Without it, we would perform slightly more copying, but that would be
   harmless.

   During each traversal, every visited variable is marked as such, so as to
   avoid being visited again. To ensure that visiting every variable once is
   enough, the young variables must be processed by increasing order of
   (initial) rank. (This is where the array [by_rank] is useful.) This
   guarantees the following property: after rank [k] has been processed, every
   visited variable has rank [k] or less already.

   Thus, while processing rank [k], every non-visited variable must have rank
   at least [k]. This explains why [k] remains constant as we go down (i.e.,
   discovering a variable [v] cannot improve the value of [k] that we are
   pushing down).

   In the presence of cycles, the upward propagation phase is incomplete, in the
   sense that it may compute ranks that are higher than necessary. For
   instance, imagine a cycle at rank 2, such that every leaf that is reachable
   from this cycle is at rank 1. Then, in principle, this cycle can be demoted
   rank 1, without loss of generality, but our upward propagation algorithm
   will not do so. *)

(* Iterating over all ranks from [base_rank] to [state.young] is in principle
   inefficient; we may waste time walking through empty buckets. In practice,
   I don't think this matters. *)

let update_ranks generation =

  (* Prepare to mark the visited equivalence classes, regardless of which
     round they were visited in. Every class is visited exactly once. *)
  let visited = fresh_mark() in

  (* Iterate over all ranks, in increasing order. *)
  for k = base_rank to state.young do

    (* A postcondition of [traverse v] is [U.rank v <= k]. (This is downward
       propagation.) [traverse v] returns the updated rank of [v]. *)
    let rec traverse v : rank =
      let data = get v in
      (* The downward propagation phase can never reach a generic variable.
         Indeed, a generic variable is never a child of an active variable. *)
      assert (data.status <> Generic);
      (* If [v] was visited before, then its rank must be below [k], as we
         adjust ranks on the way down already. *)
      if data.mark = visited then
        assert (data.rank <= k)
      else begin
        (* Otherwise, immediately mark it as visited, and immediately adjust
           its rank so as to be at most [k]. (This is important if cyclic
           graphs are allowed.) *)
        data.mark <- visited;
        adjust_rank data k;
        (* If [v] is not young, stop. If [v] is young and has structure, then
           traverse its children (updating their ranks) and on the way back
           up, adjust [v]'s rank again (this is upward propagation). If [v] is
           young and has no structure, do nothing; its rank must remain
           whatever it is. *)
        if generation.is_young v then begin
          (* The rank of this variable (before the adjustment) couldn't be
             below [k], because this variable is young and was not visited.
             Thus, it was at least [k], and because we have just adjusted it,
             it must now be [k]. *)
          assert (data.rank = k);
          (* If [v] carries some structure, ... *)
          if not (Data.is_leaf data) then
            (* ... traverse [v]'s children; on the way back up, compute the
               maximum of their ranks, and use it to adjust [v]'s rank. *)
            adjust_rank data (
              Data.fold (fun child accu ->
                max (traverse child) accu
              ) data base_rank (* the base rank is neutral for [max] *)
            )
        end
      end;
      (* In allowed cases, return [v]'s final rank. *)
      data.rank

    in
    let traverse v = ignore (traverse v) in
    List.iter traverse generation.by_rank.(k)

  done

(* -------------------------------------------------------------------------- *)

(* [generalize] examines the young variables. A variable whose rank is less
   than [state.young] is re-registered in an appropriate pool. A variable
   whose rank is [state.young] is turned into a generic variable. *)

(* [generalize] returns a list of the variables that have become generic and
   that have no structure. This list contains no duplicates, up to
   equivalence; that is, it cannot contain two variables that lie in the same
   equivalence class. *)

(* Everything is done in one loop, so the code is a bit difficult to read. We
   could use two loops: first, [List.iter] to re-register some variables and
   make some variables generic; then, [List.filter] to build the list that we
   wish to return. *)

let generalize generation : variable list =

  generation.inhabitants |> List.filter (fun v ->
    (* If [v] is not the representative element of its equivalence class,
       ignore it and drop it. If its rank is less than [state.young], there
       is no point in re-registering it at a lower level. If its rank is
       [state.young], then, by dropping it, we ensure that the list contains
       no duplicates. *)
    U.is_representative v && begin
      let data = get v in
      let r = data.rank in
      if r < state.young then begin
        (* Re-register this variable at a lower level, ... *)
        register v r;
        (* ... and drop it. It must not appear in the result list. *)
        false
      end
      else begin
        assert (r = state.young);
        (* Make this variable generic, ... *)
        data.status <- Generic;
        (* ... and keep it in the result list, if it has no structure. *)
        Data.is_leaf data
      end
    end
  )

(* -------------------------------------------------------------------------- *)

(* We are in charge of constructing and instantiating schemes, that is, graph
   fragments that contain generic (universally quantified, to-be-copied)
   variables and can have outgoing edges towards active (not-to-be-copied)
   variables. *)

(* Schemes are built when we exit the left-hand side of a [CLet] constraint,
   i.e., when we move from a context of the form [let x v = <hole> in c] to a
   context of the form [let x = scheme in <hole>]. In the general case, a
   [CLet] constraint can bind multiple term variables, so we construct zero,
   one, or more schemes. *)

(* A scheme is represented as follows. *)

type scheme = {

  (* A root variable [root]. This variable may be generic or active. *)
  root: variable;

  (* A list of all of the variables that form the generic fragment of this
     scheme. These variables are replaced with fresh active variables when
     the scheme is instantiated. The order of the variables in this list does
     not matter. Every variable in this list is generic. The rank of these
     variables is irrelevant, but it is the value of [state.young] at the
     time when [exit] was called. *)
  generics : variable array;

  (* A sublist of the variables that appear in the list [generics] and have no
     structure. These are the variables that must be explicitly universally
     quantified during elaboration to System F. The order of the variables in
     this list is chosen in an arbitrary way, and thereafter matters: it is
     exposed to the user via the functions [quantifiers] and [instantiate],
     which must agree on this order. *)
  quantifiers : variable list;

}

(* -------------------------------------------------------------------------- *)

(* Accessors. *)

(* The "body" of a type scheme is its root. *)

let body { root; _ } =
  root

(* The order in which the quantifiers appear is arbitrary but deterministic.
   Two calls to [quantifiers scheme] yield the same result. This is true even
   if the unifier is allowed to run in between, since the unifier cannot
   affect the generic part of a scheme. *)

let quantifiers { quantifiers; _ } =
  quantifiers

(* -------------------------------------------------------------------------- *)

(* Constructing schemes. *)

(* [trivial root] constructs a trivial scheme (that is, a monomorphic scheme)
   whose root is [root]. *)

let trivial root =
  let generics, quantifiers = [||], [] in
  { generics; quantifiers; root }

(* [schemify root] transforms the variable [root] into a scheme. It must be
   called after generalization has taken place, that is, after the variables
   whose rank is [state.young] have been turned into generic variables. *)

(* The set [generics] is computed as the set of generic variables that are
   reachable from [root]. (A simple graph traversal is used to find them.) The
   subset [quantifiers] can then be computed by retaining the variables in the
   list [generics] that have no structure. *)

let schemify (root : variable) : scheme =

  (* Prepare to mark which classes have been visited. *)
  let visited = fresh_mark() in
  (* Prepare to accumulate the reachable generic variables. *)
  let accu = ref [] in

  let rec traverse v =
    let data = get v in
    if data.status = Generic && data.mark <> visited then begin
      (* This variable is generic and has not yet been visited. Mark it as
         visited. Insert it into the accumulator. Traverse its descendants.
         The variable must be marked before the recursive call, so as to
         guarantee termination in the presence of cycles in the graph. *)
      data.mark <- visited;
      accu := v :: !accu;
      Data.iter traverse data
    end
  in

  (* Compute [generics]. *)
  traverse root;
  let generics = !accu in
  (* Compute [quantifiers]. *)
  let has_structure v = Data.is_leaf (get v) in
  let quantifiers = List.filter has_structure generics in
  (* Done. *)
  let generics = Array.of_list generics in
  { generics; quantifiers; root }

(* -------------------------------------------------------------------------- *)

(* We are finally in a position to define [exit]. *)

let exit ~rectypes roots =

  (* Because calls to [enter] and [exit] must be balanced, we must have: *)
  assert (state.young >= base_rank);

  (* Discover the young variables. *)
  let generation = discover_young_generation() in

  (* Update the rank of every young variable. At the end of this phase, the
     young variables that still have rank [state.young] are those that must
     become generic. *)
  update_ranks generation;

  (* If the client would like us to detect and rule out recursive types, then
     now is the time to perform an occurs check. The function that we pass as
     a parameter to [U.new_occurs_check] determines the scope of this check. A
     larger scope may allow us to detect a cycle earlier; a more limited scope
     may decrease the cost of the occurs check. The largest scope that seems
     acceptable, in terms of complexity, is the entire young generation. The
     most restricted scope that is acceptable, for soundness, is the set of
     the generalizable variables. Here, we choose the latter scope. *)
  if not rectypes then begin
    let is_generalizable data = data.rank = state.young in
    let traverse : variable -> unit = OC.new_occurs_check is_generalizable in
    List.iter traverse generation.by_rank.(state.young)
  end;

  (* Turn the young variables that still have rank [state.young] into generic
     variables, and relocate every other young variable in lower pools. This
     step produces a set [quantifiers] of all structureless generic variables.
     This set includes the quantifiers of all of the schemes that we are about
     to construct, and may also include variables that are not reachable from
     any of these schemes. *)
  let quantifiers = generalize generation in

  (* For every root in the list [roots], build a scheme. *)
  let schemes = List.map schemify roots in

  (* Empty the current pool. *)
  InfiniteArray.set state.pool state.young [];
  (* Decrement the current rank. *)
  state.young <- state.young - 1;

  (* Done. *)
  quantifiers, schemes

(* -------------------------------------------------------------------------- *)

(* Instantiation amounts to copying a fragment of a graph. The variables that
   must be copied are those in the list [generics]. This is easily done; no
   graph traversal is required. *)

let instantiate { generics; quantifiers; root } =

  (* Create a flexible copy [v'] of every generic variable [v]. At this point,
     the variable [v'] carries no structure. Record the correspondence between
     [v] and [v'] in the mapping. *)
  let mapping : variable array =
    generics |> Array.mapi (fun i v ->
      let data = get v in
      assert (data.status = Generic);
      data.mark <- i;
      flexible None
    )
  in

  (* Set up a function that maps every variable [v] to its copy. A variable
     that is not generic is mapped to itself. (We rely on the fact that if
     [v] is generic, then it must be a member of the set [generics].) *)
  let copy (v : variable) : variable =
    let data = get v in
    if data.status = Generic then
      let i = data.mark in
      mapping.(i)
    else
      v
  in

  (* For every pair of a variable [v] and its copy [v'], equip [v'] with
     a copy of the structure carried by [v]. *)
  Array.iter2 (fun v v' ->
    (get v').structure <- S.map copy (get v).structure
  ) generics mapping;

  (* Return the (instantiated) quantifiers and root. *)
  List.map copy quantifiers, copy root

(* -------------------------------------------------------------------------- *)

(* Debugging utilities. *)

let pp_status ppf s =
  match s with
  | Generic ->
      Printf.fprintf ppf "generic"
  | Flexible ->
      Printf.fprintf ppf "flexible"
  | Rigid ->
      Printf.fprintf ppf "rigid"

let show_variable v =
  let data = get v in
  Printf.printf "id = %d, rank = %d, status = %a\n"
    data.id data.rank pp_status data.status

let show_pool k =
  Printf.printf "Pool %d:\n" k;
  List.iter show_variable (InfiniteArray.get state.pool k)

let show_young () =
  Printf.printf "state.young = %d\n" state.young

let show_pools () =
  for k = base_rank to state.young do
    show_pool k
  done

let show_state () =
  show_young();
  show_pools()

end
