(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

open Signatures

module Make
  (S : OSTRUCTURE)
  (U : MUNIFIER with type 'a structure = 'a S.structure)
= struct

(* The occurs check detects cycles in the graph. It explores only a fragment
   of the graph, delimited by the user-supplied function [is_young]. *)

exception Cycle of U.variable

let new_occurs_check (is_young : U.data -> bool) =

  (* This hash table records the equivalence classes that are being visited
     (they are mapped to [false]) or have been visited (they are mapped to
     [true]). *)

  let table : (id, bool) Hashtbl.t = Hashtbl.create 128 in

  let rec traverse v =
    let data = U.get v in
    if is_young data then
      let id = S.id data in
      try
        let visited = Hashtbl.find table id in
        if not visited then
          (* This node is in the table, but has not been fully visited.
             Hence, it is being visited. A cycle has been found. *)
          raise (Cycle v)
      with Not_found ->
        (* Mark this class as being visited. *)
        Hashtbl.add table id false;
        (* Visit its successors. *)
        S.iter traverse data;
        (* Mark this class as fully visited. *)
        Hashtbl.replace table id true
  in

  traverse

end (* Make *)
