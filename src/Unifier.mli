(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

open Signatures

(**This module implements a unification algorithm for first-order terms whose
   structure is specified by the parameter [S]. In particular, the operation
   [S.conjunction] specifies how two structural constraints bearing on a
   variable are combined. *)
module Make (S : sig (** @inline *) include USTRUCTURE end)
: sig (** @inline *) include UNIFIER with type 'a structure = 'a S.structure end
