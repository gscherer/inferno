(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

open Signatures

(**This module traverses the data structure maintained by the unifier and
   offers an {i occurs check}, that is, a cycle detection algorithm. It is
   parameterized by the term structure [S] and by the unifier [U]. Read-only
   access to the unifier's data structure suffices. *)
module Make
  (S : sig (** @inline *) include OSTRUCTURE end)
  (U : sig (** @inline *) include MUNIFIER
                                  with type 'a structure = 'a S.structure end)
     : sig (** @inline *) include OCCURS_CHECK
                                  with type variable := U.variable
                                   and type data := U.data end
