(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

(* -------------------------------------------------------------------------- *)

(* We use a union-find algorithm on top of a transactional store, which offers
   mutable references as well as a notion of transaction. A transaction is
   delimited by the combinator [tentatively]. By wrapping every invocation of
   the unifier in a transaction, we are able to roll back all changes if
   unification fails. The unifier does involve a few pieces of mutable state
   that are not rolled back (e.g., the generation of fresh identifiers) but
   that is not a problem. *)

module Store =
  UnionFind.StoreTransactionalRef

module UnionFind =
  UnionFind.Make(Store)

(* -------------------------------------------------------------------------- *)

open Signatures

module Make (S : USTRUCTURE) = struct

(* -------------------------------------------------------------------------- *)

(* The data structure maintained by the unifier is as follows. *)

(* A unifier variable is a "reference" provided by the union-find algorithm. *)

(* Every equivalence class carries a structure, which itself may carry
   pointers to other variables at its leaves. This structure can be
   thought of as a constraint that bears on this equivalence class. *)

(* An @@unboxed data constructor [V] must be used because OCaml does not
   allow cyclic type abbreviations. Otherwise, we would define [variable]
   as a synonym for [data UnionFind.rref]. *)

type 'a structure =
  'a S.structure

type variable =
  | V of data UnionFind.rref [@@unboxed]

and data =
  variable structure

(* -------------------------------------------------------------------------- *)

(* We create a single "store", which means that we have at most one current
   transaction. *)

let store : data UnionFind.store =
  UnionFind.new_store()

let make data =
  UnionFind.make store data

let get (V v) =
  UnionFind.get store v

let merge f (V v1) (V v2) =
  let _ = UnionFind.merge store f v1 v2 in
  ()

let eq (V v1) (V v2) =
  UnionFind.eq store v1 v2

let is_representative (V v) =
  UnionFind.is_representative store v

let fresh structure : variable =
  V (make structure)

(* -------------------------------------------------------------------------- *)

(* The unifier uses an explicit waiting queue [q] of pending equations,
   instead of immediately dealing with these equations via recursive calls to
   [unify], because this is required for correctness. If a recursive call was
   used, it would take place after [union v1 v2] and before [set v data].
   There would be a risk for [set v data] to destroy (overwrite) information
   that has been gathered during the recursive call. *)

(* The waiting queue can be LIFO or FIFO. A LIFO queue might be faster. *)

module Q =
  Stack

type queue =
  (variable * variable) Q.t

(* [insert q v1 v2] inserts the equation [v1 = v2] into the queue [q]. *)

(* As an optimization, we do not insert this equation if the two variables
   already belong to the same equivalence class. *)

let insert q v1 v2 =
  if not (eq v1 v2) then
    Q.push (v1, v2) q

(* [unify_data q data1 data2] combines [data1] and [data2], producing a datum
   for the merged equivalence class. As a side effect, it may insert new
   pending equations into the queue [q]. *)

let unify_data q data1 data2 =
  S.conjunction (insert q) data1 data2

(* [unify q v1 v2] deals with the equation [v1 = v2]. That is, it merges the
   variables [v1] and [v2] and inserts the consequences of this equation into
   the queue [q]. Once the equation [v1 = v2] has been processed, [unify q v1
   v2] processes the pending equations in the queue [q]. If an inconsistency
   is detected, [S.InconsistentConjunction] is raised. Upon normal
   termination, a solved form has been reached. *)

(* The equivalence classes [v1] and [v2] must be merged before dealing with
   the equations produced by unifying [data1] and [data2]. This is essential
   for termination. Because we are dealing with potentially cyclic structures,
   [v1] and [v2] could appear among the children of [v1 and [v2], so the
   equation [v1 = v2] could appear again among the pending equations. Thus, we
   must merge [v1] and [v2] before we examine the pending equations. *)

let rec unify (q : queue) (v1 : variable) (v2 : variable) : unit =
  (* Merge the equivalence classes [v1] and [v2], if they are distinct, and
     combine their data using [unify_data q]. This call can insert new
     equations into [q], so as to request the unification of the children of
     [v1] and [v2]. However, it cannot alter the union-find data structure.
     Thus, the requirement of [UnionFind.merge] is obeyed. *)
  merge (unify_data q) v1 v2;
  (* Continue by dealing with the pending equations. *)
  unify_pending q

(* [unify_pending q] processes the pending equations in the queue [q]. If an
   inconsistency is detected, [S.InconsistentConjunction] is raised. Upon
   normal termination, a solved form has been reached. *)

and unify_pending (q : queue) : unit =
  match Q.pop q with
  | v1, v2 ->
      unify q v1 v2
  | exception Q.Empty ->
      ()

(* -------------------------------------------------------------------------- *)

(* This is the public version of [unify]. *)

exception Unify of variable * variable

let unify v1 v2 =
  (* Initialize an empty queue. *)
  let q = Q.create() in
  (* Wrap unification in a transaction, so that a failed unification attempt
     does not alter the state of the unifier. *)
  try
    Store.tentatively store (fun () -> unify q v1 v2)
  with S.InconsistentConjunction ->
    (* Translate [S.InconsistentConjunction] to a [Unify] exception. *)
    raise (Unify (v1, v2))

(* -------------------------------------------------------------------------- *)

end (* Make *)
