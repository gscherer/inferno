(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

open Signatures

module Option (S : GSTRUCTURE) = struct

  module S = S

  type 'a structure =
    'a S.structure option

  let map f so =
    match so with
    | None ->
        None
    | Some s ->
        Some (S.map f s)

  let iter f so =
    match so with
    | None ->
        ()
    | Some s ->
        S.iter f s

  let fold f so accu =
    match so with
    | None ->
        accu
    | Some s ->
        S.fold f s accu

  (* If one of the two structures is undefined, we keep the other. If
     both are defined, we conjoin them. *)

  exception InconsistentConjunction =
    S.InconsistentConjunction

  let conjunction equate so1 so2 =
    match so1, so2 with
    | None, so
    | so, None ->
        so
    | Some s1, Some s2 ->
        let s = S.conjunction equate s1 s2 in
        if s == s1 then so1 else Some s

  let pprint elem os =
    PPrint.OCaml.option (S.pprint elem) os

end (* Option *)
