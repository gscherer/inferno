#!/bin/bash
set -euo pipefail
shopt -s extglob

# This script helps make sense of the error messages produced by odoc.

# A list of missing modules.
missing=""

while read line
do
  buffer="$line"
  case "$line" in
  File*client*:)
    # A warning about the directory client/. Stop.
    echo "Skipping all warnings about client/."
    break
    ;;
  File*:)
    read line
    buffer="$buffer$line"
    case "$line" in

    "Warning: Couldn't find the following modules:")
      read line
      # A warning that some modules could not be found.
      # Accumulate the names of the missing modules.
      missing="$missing $line"
      ;;

    "Warning: Failed to lookup type unresolvedroot(Stdlib)"*)
      # A warning about Stdlib. Skip.
      echo "Skipping warnings about Stdlib."
      ;;

    "Warning: While"*)
      read line
      buffer="$buffer$line"
      case "$line" in
      "Failed to lookup type unresolvedroot(Stdlib)"*)
        # A multi-line warning about Stdlib. Skip.
        echo "Skipping warnings about Stdlib."
        ;;
      *)
        # Another kind of warning. Echo.
        echo "$buffer"
        ;;
      esac
      ;;

    *)
      # Another kind of warning. Echo.
      echo "$buffer"
      ;;
    esac
    ;;

  *)
    echo "$buffer"
    ;;
  esac
done

# Show the missing modules.
echo "Missing modules:"
echo "$missing" | sed -e 's/^ *//' | tr " " "\n" | sort | uniq
